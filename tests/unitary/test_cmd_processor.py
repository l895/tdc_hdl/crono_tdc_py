from crono_tdc_py.cmd_processor import CMD_ID, CronoTDCCommandProcessor
import pytest

@pytest.mark.parametrize(
    "cmd_name, expected_name, expected_id, expected_type",
    [
        ('read_crono_tdc_core_version', 'read_crono_tdc_core_version', CMD_ID.READ_CRONO_TDC_CORE_VERSION, 'read_register'),
        ('measure', 'measure', CMD_ID.MEASURE, 'execute'),
        ('read_time_windows_us', 'read_time_windows_us', CMD_ID.READ_TIME_WINDOWS_US, 'read_register'),
        ('write_time_windows_us', 'write_time_windows_us', CMD_ID.WRITE_TIME_WINDOWS_US, 'write_register'),
        ('read_channels_enabled', 'read_channels_enabled', CMD_ID.READ_CHANNELS_ENABLED, 'read_register'),
        ('write_channels_enabled', 'write_channels_enabled', CMD_ID.WRITE_CHANNELS_ENABLED, 'write_register')
    ]
)
def test_look4cmd(cmd_name, expected_name, expected_id, expected_type):
    cmd_processor = CronoTDCCommandProcessor()
    returned_cmd = cmd_processor.look4cmd(cmd_name)
    assert returned_cmd.name == expected_name, f"command name returned is not the expected! {expected_name} != {returned_cmd.name}"
    assert returned_cmd.id == expected_id, f"command id returned is not the expected! {expected_id} != {returned_cmd.id}"


@pytest.mark.parametrize(
    "cmd_name, cmd_data, expected_built_cmd",
    [
        ('read_crono_tdc_core_version', b'',        CMD_ID.READ_CRONO_TDC_CORE_VERSION.to_bytes(1, 'big')),
        ('read_crono_tdc_core_version', b'\x50',    b'\x50'+CMD_ID.READ_CRONO_TDC_CORE_VERSION.to_bytes(3, 'big')),
        ('read_time_windows_us', b'\x35\x89\x11',   b'\x35\x89\x11'+CMD_ID.READ_TIME_WINDOWS_US.to_bytes(3, 'big')),
        ('write_time_windows_us', b'\x10\x59\x01',  b'\x10\x59\x01'+CMD_ID.WRITE_TIME_WINDOWS_US.to_bytes(3, 'big')),
    ]
)
def test_build_cmd(cmd_name, cmd_data, expected_built_cmd):
    cmd_processor = CronoTDCCommandProcessor()
    cmd_built = cmd_processor.build_cmd(cmd_name, cmd_data)
    assert cmd_built == expected_built_cmd, f"command built is not the expected one! {cmd_built} != {expected_built_cmd}"
