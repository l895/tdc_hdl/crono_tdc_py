from crono_tdc_py.hdlc_lite_processor import HDLCLiteProcessor
import pytest

@pytest.mark.parametrize(
    "data, expected_frame",
    [
        (b'hola_como_estas', b'\x7E'+b'hola_como_estas'+b'\xB2\x50'+b'\x7E'),
        (b'0123456789ABCDEF!!2312#@!', b'\x7E'+b'0123456789ABCDEF!!2312#@!'+b'\x59\x1A'+b'\x7E'),
        (b'', b'\x7E'+b''+b'\x00\x00'+b'\x7E'),
        (b'66asdCASDfa?!ads', b'\x7E'+b'66asdCASDfa?!ads'+b'\x81\x59'+b'\x7E'),
        (b'achua!', b'\x7E'+b'achua!'+b'\x0E\x07'+b'\x7E'),
    ]
)
def test_build_frame(data, expected_frame):
    hdlc_lite_processor = HDLCLiteProcessor('/dev/fakeuart', 115200, 1, 1)
    returned_frame = hdlc_lite_processor.build_frame(data)
    assert returned_frame == expected_frame, f"frame built is not equal to the expected! {returned_frame} != {expected_frame}"
