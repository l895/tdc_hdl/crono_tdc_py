import pytest

def pytest_addoption(parser):
    parser.addoption(
        '--crono-tdc-serial-port', action='store', default='/dev/ttyUSB0', help='Crono TDC Serial port'
    )

@pytest.fixture
def crono_tdc_serial_port(request):
    return request.config.getoption("--crono-tdc-serial-port")