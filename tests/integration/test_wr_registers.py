## pytest used to test registers interface
from crono_tdc_py.crono_tdc import CronoTDC
import logging
import pytest
from time import sleep

logging.basicConfig(format='[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',datefmt='%m-%d %H:%M:%S',level=logging.DEBUG)


def test_time_windows_us_reg(crono_tdc_serial_port):
    """
    Test if the TIME_WINDOWS_US register can be written
    and readed correctly.
    """
    logging.info(f"using {crono_tdc_serial_port} to communicate with Crono TDC...")
    crono_tdc = CronoTDC(crono_tdc_serial_port) ## FIXME! generalize this
    time_windows_range_test = range(10,1048576, 50000)
    case_counter = 0
    for i in time_windows_range_test:
        case_counter += 1
        logging.info(f"\n\n########## CASE {case_counter}/{len(time_windows_range_test)} ##########")
        logging.info(f"{case_counter}/{len(time_windows_range_test)}: setting {i} us to TIME_WINDOWS_US register...")
        crono_tdc.time_windows_us = i
        sleep(0.1)
        assert crono_tdc.time_windows_us == i, f"time windows could not be set! {crono_tdc.time_windows_us} != {i}"
        logging.info(f"{case_counter}/{len(time_windows_range_test)}: time windows could be set successfully!")


def test_channels_enabled_reg(crono_tdc_serial_port):
    """
    Test if the CHANNELS_ENABLED register can be written
    and readed correctly.
    """
    logging.info(f"using {crono_tdc_serial_port} to communicate with Crono TDC...")
    crono_tdc = CronoTDC(crono_tdc_serial_port) ## FIXME! generalize this
    channels_enabled_range_test = range(0,0b11111,1)
    case_counter = 0
    for i in channels_enabled_range_test:
        case_counter += 1
        logging.info(f"\n\n########## CASE {case_counter}/{len(channels_enabled_range_test)} ##########")
        logging.info(f"{case_counter}/{len(channels_enabled_range_test)}: setting {i} us to CHANNELS_ENABLED register...")
        crono_tdc.channels_enabled = i
        sleep(0.1)
        assert crono_tdc.channels_enabled == i, f"channels enabled could not be set! {crono_tdc.channels_enabled} != {i}"
        logging.info(f"{case_counter}/{len(channels_enabled_range_test)}: channels enabled could be set successfully!")