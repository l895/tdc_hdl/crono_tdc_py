# Crono TDC Py

This is a Python library to communicate with Crono TDC core through a serial port.
This library implements HDLC-lite over UART protocol to send commands to the TDC and receive responses.
Also, have some useful functions to plot the data read from the TDC.

## Installation

You can install this library as a python package (cloning repo and installing with pip or installing with pip from repo in an atomic command):

```bash
git clone git@gitlab.com:l895/tdc_hdl/crono_tdc_py.git
cd crono_tdc_py && pip3 install .
```

Also, you can use the docker image that have installed the package.
For ARM64 hosts (raspberry for example) you can use: `registry.gitlab.com/l895/tdc_hdl/crono_tdc_py/arm64v8/v1.0.0`.
For AMD64 hosts (common PCs) you can use `registry.gitlab.com/l895/tdc_hdl/crono_tdc_py/amd64/v1.0.0`.

## Usage

You can use this library in your scripts in the following way:

```python
from crono_tdc_py.crono_tdc import CronoTDC

if __name__ == "__main__":
    crono_tdc = CronoTDC('/dev/ttyUSB0')

    # library reads core version from TDC register through the serial port
    core_version = crono_tdc.crono_tdc_core_version

    # library read TDC configuration registers through serial port
    time_windows_us = crono_tdc.time_window_us
    enabled_channels_mask = crono_tdc.channels_enabled

    # library writes TDC configuration register through serial port
    crono_tdc.channels_enabled = 0b001

    # library sends the command that starts a measurement to TDC through serial port
    crono_tdc.measure()

    # library sends the command to read data from TDC RAMs and store it in a CSV file
    crono_tdc.read_channels_data('path/to/csv_file.csv')
```

For more examples you can explore `scripts/crono_tdc_gen_experiment.py` script. 
This script was used to perform an essay to test TDC resolution, triggering `crono signal generator` through raspberry GPIOs.
