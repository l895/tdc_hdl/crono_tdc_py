import pandas as pd
import matplotlib.pyplot as plt
from cycler import cycler
import os
import numpy as np
import argparse
import logging
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

LOG_LEVEL=logging.INFO
logging.basicConfig(format='[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',datefmt='%m-%d %H:%M:%S',level=LOG_LEVEL)

##############################################################################
## Constant values
ASSUMED_RISE_TIME_DIVISOR=1000

PLOT_STYLE = {
        'axes.axisbelow': True,
        'axes.edgecolor': 'white',
        'axes.facecolor': '#EAEAF2',
        'axes.grid': True,
        'axes.labelcolor': '.15',
        'axes.linewidth': 0.0,
#        'axes.prop_cycle': cycler('color', ['#7A76C2', '#ff6e9c98', '#f62196', '#18c0c4', '#f3907e', '#66E9EC']),
        'axes.prop_cycle': cycler('color', ["#392682", "#7a3a9a", "#3f86bc", "#28ada8", "#83dde0"]),
        'figure.facecolor': 'fefeff',
        'savefig.facecolor': 'fefeff',
        'font.family': ['sans-serif'],
        'font.sans-serif': [
            'Overpass', 'Helvetica', 'Helvetica Neue',
            'Arial', 'Liberation Sans', 'DejaVu Sans',
            'Bitstream Vera Sans', 'sans-serif'
            ],
        'grid.color': 'white',
        'grid.linestyle': '-',
        'image.cmap': 'RdPu',
        'legend.frameon': True,
        'legend.numpoints': 1,
        'legend.scatterpoints': 1,
        'legend.facecolor': '#EAEAF2',
        'legend.edgecolor': '#cacad0',
        'lines.solid_capstyle': 'round',
        'text.color': '.15',
        'xtick.color': '.15',
        'xtick.labelsize': 12,
        'xtick.direction': 'out',
        'xtick.major.size': 2,
        'xtick.minor.size': 2,
        'ytick.color': '.15',
        'ytick.direction': 'out',
        'ytick.major.size': 2,
        'ytick.minor.size': 2,
        }


##############################################################################
## Plot functions
def process_channels_data(df: pd.DataFrame, tdc_clock_freq_mhz:int) -> pd.DataFrame:
    """
    Take a dataframe with the data registers of a channel and convert that data
    to points that could be plotted.
    The dataframe must have the following columns:
            | clock_cycle_num | iserdese_out | tag |
    """
    tdc_clock_period_ns = 1000/tdc_clock_freq_mhz
    transformed_df = pd.DataFrame(columns=[
        'time_ns', 'value', 'true_bit', 'tag'
    ])

    for index, row in df.iterrows():
        logging.info(f"analyzing row: clock_cycle_num: {row['clock_cycle_num']}, iserdese_out: 0b{row['iserdese_out']:04b}")
        time_offset_in_measurement = row['clock_cycle_num'] * tdc_clock_period_ns

        points_counter = 0

        ## Infere last point of last clock cycle using tag information
        previous_clock_period_last_point_time_ns = time_offset_in_measurement-tdc_clock_period_ns*(1/ASSUMED_RISE_TIME_DIVISOR)
        if (row['tag'] == 'only_small_pulse'):
            points_counter += 1
            logging.info(f"{points_counter} (infered): time_ns: {previous_clock_period_last_point_time_ns}, value: {0}, true_bite: False, tag: {row['tag']}")
            infered_points = {
                            'time_ns': [ previous_clock_period_last_point_time_ns ],
                            'value': [ 0],
                            'true_bit': [ False ],
                            'tag': [ row['tag'] ]
                        }
        elif row['tag'].startswith('long_pulse_ending'):
            points_counter += 1
            logging.info(f"{points_counter} (infered): time_ns: {previous_clock_period_last_point_time_ns}, value: {1}, true_bite: False, tag: {row['tag']}")
            infered_points = {
                            'time_ns': [ previous_clock_period_last_point_time_ns ],
                            'value': [ 1 ],
                            'true_bit': [ False ],
                            'tag': [ row['tag'] ]
                        }
        elif row['tag'].startswith('long_pulse_starting'):
            points_counter += 1
            logging.info(f"{points_counter} (infered): time_ns: {previous_clock_period_last_point_time_ns}, value: {0}, true_bite: False, tag: {row['tag']}")
            infered_points = {
                            'time_ns': [ previous_clock_period_last_point_time_ns ],
                            'value': [ 0 ],
                            'true_bit': [ False ],
                            'tag': [ row['tag'] ]
                        }

        transformed_df = pd.concat([transformed_df, pd.DataFrame(infered_points)], ignore_index=True, axis=0)

        for i in range(4):
            # for each ISERDESE bit we generate 2 plot-points
            # one corresponde to the beginning of the bit and the another to the end
            bit_val = (row['iserdese_out'] >> (3-i)) & 0x1
            logging.info(f"bit_val: {bit_val} ({i})")
            time_offset_in_clock_cycle = i * (tdc_clock_period_ns/4)
            bit_time_ns = time_offset_in_clock_cycle + time_offset_in_measurement
            points_counter += 1
            logging.info(f"{points_counter}: time_ns: {bit_time_ns}, value: {bit_val}, true_bite: True, tag: {row['tag']}")
            points_counter += 1
            logging.info(f"{points_counter}: time_ns: {bit_time_ns+tdc_clock_period_ns/4-2*tdc_clock_period_ns/ASSUMED_RISE_TIME_DIVISOR}, value: {bit_val}, true_bite: False, tag: {row['tag']}")
            single_bit_points = {
                            'time_ns': [
                                bit_time_ns,
                                bit_time_ns+tdc_clock_period_ns/4-2*tdc_clock_period_ns/ASSUMED_RISE_TIME_DIVISOR
                            ],
                            'value': [
                                bit_val,
                                bit_val,
                            ],
                            'true_bit': [
                                True,
                                False,
                            ],
                            'tag': [
                                row['tag'],
                                row['tag'],
                            ]
                        }

            transformed_df = pd.concat([transformed_df, pd.DataFrame(single_bit_points)], ignore_index=True, axis=0)

        ## Infere last point of current clock cycle using tag information
        current_clock_period_last_point_time_ns = time_offset_in_measurement+tdc_clock_period_ns-tdc_clock_period_ns*(1/ASSUMED_RISE_TIME_DIVISOR)
        if (row['tag'] == 'only_small_pulse'):
            points_counter += 1
            logging.info(f"{points_counter} (infered): time_ns: {current_clock_period_last_point_time_ns}, value: {0}, true_bite: False, tag: {row['tag']}")
            infered_points = {
                            'time_ns': [ current_clock_period_last_point_time_ns ],
                            'value': [ 0 ],
                            'true_bit': [ False ],
                            'tag': [ row['tag'] ]
                        }
        elif (row['tag'] == 'long_pulse_ending') or (row['tag'] == 'long_pulse_ending_small_appears'):
            points_counter += 1
            logging.info(f"{points_counter} (infered): time_ns: {current_clock_period_last_point_time_ns}, value: {0}, true_bite: False, tag: {row['tag']}")
            infered_points = {
                            'time_ns': [ current_clock_period_last_point_time_ns ],
                            'value': [ 0 ],
                            'true_bit': [ False ],
                            'tag': [ row['tag'] ]
                        }
        elif (row['tag'] == 'long_pulse_ending_long_starting') or (row['tag'].startswith('long_pulse_starting')):
            points_counter += 1
            logging.info(f"{points_counter} (infered): time_ns: {current_clock_period_last_point_time_ns}, value: {1}, true_bite: False, tag: {row['tag']}")
            infered_points = {
                            'time_ns': [ current_clock_period_last_point_time_ns ],
                            'value': [ 1 ],
                            'true_bit': [ False ],
                            'tag': [ row['tag'] ]
                        }

        transformed_df = pd.concat([transformed_df, pd.DataFrame(infered_points)], ignore_index=True, axis=0)

    transformed_df.sort_values('time_ns', inplace=True)
    logging.info(transformed_df.head())

    return transformed_df

def plot_data_from_single_channel(chn:int,channel_data_processed_df: pd.DataFrame, tdc_clock_freq_mhz: int, ax):
    """
    Plot data from a single channel in a pyplot axes

    Parameters
    ----------
    chn : int
        The channel number.

    channel_data_processed_df : pd.DataFrame()
        Dataframe with the processed data.
        Columns:
        |time_ns|value|true_bit|tag|

    tdc_clock_freq_mhz: int
        TDC frequency in MHz.

    ax:
        Pyplot axis where the waveform will be plotted.
    """
    channel_data_only_true_bits_df = channel_data_processed_df[ channel_data_processed_df['true_bit'] == True ]

    ## plot waveform
    ax.plot(channel_data_processed_df['time_ns'], channel_data_processed_df['value'], label='Reconstructed waveform')
    dfn = channel_data_processed_df.apply(pd.to_numeric, errors='ignore')
    ax.fill_between(dfn['time_ns'], dfn['value'], alpha=0.1)
    xleft_lim = channel_data_processed_df['time_ns'].min()
    xright_lim = channel_data_processed_df['time_ns'].max()
    logging.info(f"first data appears at {xleft_lim} ns")

    ## plot true measured points
    markers, stems, base = ax.stem(
        channel_data_only_true_bits_df['time_ns'],
        channel_data_only_true_bits_df['value'],
        markerfmt='x',
        linefmt='--',
        basefmt=" ", # hide baseline
        label='ISERDESE2 points',
    )
    plt.setp(stems, 'linewidth', 1)
    plt.setp(stems, 'color', 'indianred')
    plt.setp(stems, 'alpha', 0.5)
    plt.setp(markers, 'linewidth', 1)
    plt.setp(markers, 'color', 'darkred')

    ax.set_ylabel(f"channel {chn}", fontsize=14)
    ax.set_ylim(bottom=0, top=1.05)
    ax.set_xlim(left=xleft_lim, right=xright_lim)
    ax.grid(visible=True, which='both', axis='both', alpha=0.5)
    ax.set_xlabel('time [ns]', fontsize=14)
    ax.yaxis.set_ticks(ticks=[0,1]) # set only 0 an 1 as y ticks
    #ax.locator_params(axis='x', nbins=20)

    tdc_clock_period_ns = 1000/tdc_clock_freq_mhz
    tdc_bin_size_ns = tdc_clock_period_ns/4
    ax.xaxis.set_major_locator(MultipleLocator(tdc_clock_period_ns))
    ax.xaxis.set_major_formatter('{x:.0f}')
    ax.tick_params(which='major', length=2, width=2)
    ax.tick_params(which='minor', length=2, width=1)
    plot_interval_time_ns = (xright_lim - xleft_lim)
    bins_in_plot = (plot_interval_time_ns / tdc_bin_size_ns)
    if bins_in_plot <= 14:
        logging.info(f'In this plot we have less than 14 tdc bins! bins = {bins_in_plot} ({plot_interval_time_ns} ns)')
        ax.xaxis.set_minor_locator(MultipleLocator((tdc_bin_size_ns)))
        ax.xaxis.set_minor_formatter('{x:.0f}' if (tdc_bin_size_ns).is_integer() else '{x:.1f}')
        ax.tick_params(axis='x', which='both', labelsize=13)
    elif bins_in_plot <= 25:
        logging.info(f'In this plot we have more than 14 bins and less than 25 bins! bins = {bins_in_plot} ({plot_interval_time_ns} ns)')
        ax.xaxis.set_minor_locator(MultipleLocator((tdc_bin_size_ns*2)))
        ax.xaxis.set_minor_formatter('{x:.0f}' if (tdc_bin_size_ns*2).is_integer() else '{x:.1f}')
        ax.tick_params(axis='x', which='both', labelsize=13)
    else:
        logging.info(f'In this plot we have more than 25 tdc bins! Autoscaling xtick number. Bins = {bins_in_plot} ({plot_interval_time_ns} ns)')
        ax.tick_params(axis='x', which='both', labelsize=12)


    max_time_ns = channel_data_processed_df.loc[channel_data_processed_df['time_ns'].idxmax()]['time_ns']
    logging.info(f"channel {chn} max time measure: {max_time_ns} ns")
    ax.vlines(x=np.arange(0,max_time_ns,1000/tdc_clock_freq_mhz), ymin=0, ymax=1.2, ls='--', lw=1, alpha=0.5, colors='cadetblue')



def plot_channels_data_from_csv(csv_filepath:str, tdc_clock_freq_mhz:int, show:bool = False):
    """
    Read a CSV with the following columns:
        | channel | data_register | clock_cycle_num | iserdese_out | tag |
    and plot the channels data.

    Parameters
    ----------
    csv_filepath : str
        Path of the csv that have stored the channels data.

    tdc_clock_freq_mhz: int
        TDC frequency in MHz.

    show:
        Set this parameter to true if you want to open a window of matplotlib to preview the graph.
    """
    df = pd.read_csv(csv_filepath)
    df.sort_values('clock_cycle_num', inplace=True)

    channels_in_df = list(df['channel'].unique())
    channels_in_df.sort()
    if len(channels_in_df) < 1:
        logging.info(f"there is no channel in csv! exiting...")
        return

    title = os.path.splitext(csv_filepath)[0]

    fig, ax = plt.subplots(nrows=len(channels_in_df))
    fig.suptitle(title, fontsize=16,)

    axis_counter = 0
    for chn in channels_in_df:
        channel_df = df[ df['channel'] == chn ]
        logging.info(f"## CHANNEL {chn} ##############################################################")
        logging.info(f"channel {chn} dataframe:")
        logging.info(channel_df.head())

        # process channel dataframe and store processed data
        channel_data_processed_df = process_channels_data(channel_df, tdc_clock_freq_mhz)
        channel_waveform_df_name = os.path.splitext(csv_filepath)[0] + f"_processed_chn{chn}.csv"
        channel_data_processed_df.to_csv(channel_waveform_df_name)

        plot_data_from_single_channel(
            chn=chn,
            channel_data_processed_df=channel_data_processed_df,
            tdc_clock_freq_mhz=tdc_clock_freq_mhz,
            ax=ax[axis_counter]
            )

        if axis_counter >= len(channels_in_df)-1:
            logging.info("Plotting textbox!!!")

            plt.subplots_adjust(bottom=0.1, right=0.82, left=0.05, top=0.95)
            ax[0].text(
                    x=1.02, y=0.2,
                    s=f"Notes:\nTDC base frequency: {tdc_clock_freq_mhz} MHz\nTDC bin size: {(1000/tdc_clock_freq_mhz)/4} ns",
                    transform=ax[0].transAxes,
                    fontsize=14,
                    horizontalalignment='left',
                    verticalalignment='bottom',
                    bbox={'boxstyle':'round', 'facecolor':'#EAEAF2', 'alpha':0.5, 'edgecolor':'#cacad0'}
                )
            ax[0].legend(
                    bbox_to_anchor=(1.01, 0.5),
                    fontsize=14,
                    loc='lower left'
                )

        axis_counter += 1

    if show:
        plt.show(block=True)
    fig.savefig(f"{title}.png")


def generate_pg_reference_signal_csv(points:int, points_qty:int, freq_mhz:int, chn:int, csv_filename:str = "reference_signal.csv"):
    """
    Generate the CSV with TDC pattern generator syntethic signal.
    TDC have pattern generator output in it's own that generate a synthetic signal when
    a measure is triggered. With this function we can build the syntethic signal in a .csv
    file using the pattern and frequency as inputs.

    Parameters
    ----------
    points : int
        Pattern generator points in integer format.
    
    points_qty: int
        Quantity of points (bits) of pattern generator.

    freq_mhz: int
        Frequency (in MHz) of pattern generator.

    chn: int
        Channel input that will measure this.

    csv_filename: str
        Name of the csv that will store the synthethic signal.
    """
    clock_period_ns = 1000/freq_mhz
    time_ns_list = []
    value_list = []

    logging.info(f"Generating reference signal csv for points {points} (qty: {points_qty}) and frequency {freq_mhz} MHz in {csv_filename} file...")

    current_time_ns = 0
    for i in range(points_qty):
        shift_value = points_qty-1-i
        value = (points >> shift_value) & 0x1
        init_time_ns = current_time_ns
        end_time_ns = current_time_ns+clock_period_ns*(1-1/ASSUMED_RISE_TIME_DIVISOR)

        time_ns_list.extend([init_time_ns, end_time_ns])
        value_list.extend([value, value])

        logging.info(f"{(shift_value):4d}: init_time_ns: {init_time_ns:10f} ns, value: {value}")
        logging.info(f"{(shift_value):4d}: end_time_ns:  {end_time_ns:10f} ns, value: {value}")

        current_time_ns += clock_period_ns

    df = pd.DataFrame(  {'channel': chn, 'time_ns': time_ns_list, 'value': value_list},
                        columns = ['channel', 'time_ns', 'value']
    )

    df.to_csv(csv_filename)

###############################################################################
## Misc
def parse_arguments():
    parser = argparse.ArgumentParser(description="Tool used to plot Crono TDC output data.")
    parser.add_argument('--tdc-data-file', required=True, type=str, help="Input data file path.")
    parser.add_argument('--tdc-freq-mhz', required=True, type=int, help="TDC frequency in MHz.")
    parser.add_argument('--show', action='store_true', help="Show a preview of the graph before storing csv")
    return parser.parse_args()

###############################################################################
## Main
def cli():
    args = parse_arguments()
    
    plt.style.use(PLOT_STYLE)
    plot_channels_data_from_csv(args.tdc_data_file, args.tdc_freq_mhz, show=args.show)

if __name__ == "__main__":
    cli()
