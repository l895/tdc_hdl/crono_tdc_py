from crono_tdc_py.crono_tdc import CronoTDC
import logging
from time import sleep
from datetime import datetime
import argparse
from multiprocessing import Process
import RPi.GPIO as GPIO
import os
import sys

##############################################################################
## Constant values
EXPECTED_CRONO_TDC_CORE_VERSIONS=(b'318c6a9')
RPI_GPIOS = (26)#(6,13,16,26)
OUTPUT_DIR=os.getcwd()+"/experiment_outputs"
EXPERIMENT_DATE=datetime.now().strftime("%Y%m%d_%H%M%S")
OUTPUT_CSV_FILENAME=f"{EXPERIMENT_DATE}_crono_tdc_experiment.csv"
DEFAULT_CHANNELS_MASK=0x1
DEFAULT_TIME_WINDOWS_US=5000000

##############################################################################
## Configs
GPIO.setmode(GPIO.BCM)

LOG_LEVEL=logging.DEBUG
#logging.basicConfig(format='[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',datefmt='%m-%d %H:%M:%S',level=LOG_LEVEL)

log_formatter = logging.Formatter(fmt='[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',datefmt='%m-%d %H:%M:%S')
root_logger = logging.getLogger()#__name__)
root_logger.setLevel(LOG_LEVEL)

logger_fh = logging.FileHandler(OUTPUT_DIR+f"/{EXPERIMENT_DATE}_crono_tdc_experiment.log")
logger_fh.setLevel(LOG_LEVEL)
logger_fh.setFormatter(log_formatter)
root_logger.addHandler(logger_fh)

logger_console = logging.StreamHandler(stream = sys.stdout)
logger_console.setLevel(LOG_LEVEL)
logger_console.setFormatter(log_formatter)
root_logger.addHandler(logger_console)

###############################################################################
## Crono TDC Functions
def crono_tdc_aliveness(crono_tdc: CronoTDC) -> None:
    root_logger.info("Checking if TDC is alive and if the Core versions is the correct one.")
    core_version = bytes(crono_tdc.crono_tdc_core_version)
    assert core_version != b'', f"Crono TDC do not answer nothing!"
    assert core_version in EXPECTED_CRONO_TDC_CORE_VERSIONS, f"Crono TDC Version is not the expected one! {core_version} is not one of this ({EXPECTED_CRONO_TDC_CORE_VERSIONS})"

    root_logger.info(f'Crono TDC Core version: {core_version}')

def crono_tdc_config(crono_tdc:CronoTDC, desired_time_windows_us:int, desired_channels_enabled_mask:int) -> None:
    root_logger.info(f"Configuring time windows to {desired_time_windows_us}")
    previous_time_windows_us = crono_tdc.time_windows_us
    crono_tdc.time_windows_us = desired_time_windows_us
    actual_time_windows_us = crono_tdc.time_windows_us
    assert actual_time_windows_us == desired_time_windows_us, f"Time windows could not be set! Previous: {previous_time_windows_us}, actual: {actual_time_windows_us}, desired: {desired_time_windows_us}"
    root_logger.info(f"Time windows was set to {desired_time_windows_us} us!") 

    root_logger.info(f"Configuring enabled channels mask to {desired_channels_enabled_mask}")
    previous_channels_enabled_mask = crono_tdc.channels_enabled
    crono_tdc.channels_enabled = desired_channels_enabled_mask
    actual_channels_enabled_mask = crono_tdc.channels_enabled
    assert actual_channels_enabled_mask == desired_channels_enabled_mask, f"Channels enabled mask could not be set! Previous: {previous_channels_enabled_mask}, actual: {actual_channels_enabled_mask}, desired: {desired_channels_enabled_mask}"
    root_logger.info(f"Channels enabled mask was set to {desired_channels_enabled_mask}!")

def crono_tdc_start_measure(crono_tdc: CronoTDC) -> None:
    root_logger.info("Starting Crono TDC Measure...")
    crono_tdc.measure()

def crono_tdc_retrieve_data(crono_tdc: CronoTDC) -> None:
    os.makedirs(OUTPUT_DIR, exist_ok=True)
    output_filepath=os.path.join(OUTPUT_DIR, OUTPUT_CSV_FILENAME)

    root_logger.info(f"Reading measurement data from TDC and storing it into {output_filepath}")
    crono_tdc.read_channels_data(output_filepath)

###############################################################################
## Signal Generator Functions
def signal_generator_gen_pattern(rpi_gpios:tuple):
    # Signal generation is done by another FPGA board controlled by GPIOs
    root_logger.info(f"Starting signal generation! (gpios: {rpi_gpios})")
    GPIO.setup(rpi_gpios, GPIO.OUT, initial=GPIO.LOW)    
    GPIO.output(rpi_gpios, GPIO.LOW)
    sleep(100e-6)
    GPIO.output(rpi_gpios, GPIO.HIGH)

###############################################################################
## Misc
def parse_arguments():
    parser = argparse.ArgumentParser(description="Arty's Crono TDC and Crono Signal generator experiment.")
    parser.add_argument('--serial', type=str, help="Serial port where is connected the Crono TDC.")
    parser.add_argument('--time-window-us', default=DEFAULT_TIME_WINDOWS_US, type=int, help="Width of the time window.")
    parser.add_argument('--channels-mask', default=DEFAULT_CHANNELS_MASK, type=int, help="Channels enabled mask.")
    parser.add_argument('--rpi-gen-gpios', default=RPI_GPIOS, type=int, nargs="+", help="RPI gpios used to trigger the signal generator")

    return parser.parse_args()

###############################################################################
## Main
def cli():
    """
    1) Launch a measure with Crono TDC talking to it through serial port.
    2) Launch signal generation with siggen using RPI GPIOs.
    3) Download data.
   """
    args = parse_arguments()

    crono_tdc = CronoTDC(args.serial)

    crono_tdc_aliveness(crono_tdc)

    sleep(1)

    crono_tdc_config(crono_tdc, args.time_window_us, args.channels_mask)
    
    sleep(1)

    root_logger.info('Starting measure with TDC and generating signal afterwards')
    meas = Process(target=crono_tdc.measure)
    meas.start()
    sleep(1) ## super important, if not, we can generate the pulses before the measure is started!
    signal_generator_gen_pattern(args.rpi_gen_gpios)
    meas.join()
    
    sleep(1)

    crono_tdc_retrieve_data(crono_tdc)

    root_logger.info("This is the end my friend!")

if __name__ == "__main__":
    cli()

