import shlex
from subprocess import check_output, CalledProcessError
from setuptools import setup, find_packages
import pathlib


here = pathlib.Path(__file__).parent.resolve()
long_description = (here / "README.md").read_text(encoding="utf-8")


def git_to_pep440(git_version):
    if '-' not in git_version:
        return git_version

    sep = git_version.index('-')
    version = git_version[:sep] + '+dev' + git_version[sep+1:].replace('-', '.')
    return version

def git_version():
    cmd = 'git describe --tags --always --dirty --match v[0-9]*'
    try:
        git_version = check_output(shlex.split(cmd)).decode('utf-8').strip()[1:]
    except CalledProcessError as e:
        raise Exception('Could not get git version') from e
    return git_to_pep440(git_version)


setup(
    name="crono_tdc_py",
    version=git_version(),
    description="Tool used to communicate and operate a Crono TDC.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/l895/tdc_hdl/crono_tdc_py",
    author="Julian Rodriguez",
    author_email="jnrodriguez@estudiantes.unsam.edu.ar",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        'Operating System :: Linux',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Utilities',
    ],
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'crono_tdc_plot=scripts.crono_tdc_plot:cli',
            'crono_tdc_gen_experiment=scripts.crono_tdc_gen_experiment:cli'
            ]
   },
    python_requires=">=3.7, <4",
    install_requires=["pyserial", "crc==1.2.0", "pandas", "matplotlib", "pytest"]
)
