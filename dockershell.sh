#!/bin/bash

###############################################################################
## Function
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m'

log_info() {
	        echo -e ${GREEN}"$(date +"%Y-%m-%dT%H:%M:%S.%03N") - $*"${NC}
	}

log_error() {
	        echo -e ${RED}"$(date +"%Y-%m-%dT%H:%M:%S.%03N") - $*"${NC}
	}

log_warning() {
	        echo -e ${ORANGE}"$(date +"%Y-%m-%dT%H:%M:%S.%03N") - $*"${NC}
	}

###############################################################################
## Parameters
DOCKER_IMAGE_EXECUTED_LOCALLY='crono_tdc_py_dev:local'
REBUILD_IMAGE=false

while getopts "r" opt; do
  case ${opt} in
	r)
        REBUILD_IMAGE=true
      	;;
 	\?)
		echo "Invalid option: -$OPTARG"
		exit 1
		;;
	:)
        echo "The option -$OPTARG requires an argument."
        exit 1
      	;;
  esac
done

if [ "$(arch)" = "x86_64" ]; then
	ARCH=amd64
	TARGET=crono_tdc_py_dev
elif [ "$(arch)" = "aarch64" ]; then
	ARCH=arm64v8
	TARGET=raspi_dev
else
	log_error "Platform not supported: $(arch)"
	exit -1
fi

log_info "Platform: $(arch) (${ARCH})"

if [ "${REBUILD_IMAGE}" = "true" ]; then
	log_warning "erasing ${DOCKER_IMAGE_EXECUTED_LOCALLY}..."	
	docker rmi -f ${DOCKER_IMAGE_EXECUTED_LOCALLY}
fi

if [[ "$(docker images -q ${DOCKER_IMAGE_EXECUTED_LOCALLY} 2> /dev/null)" == "" ]]; then
	log_info "${DOCKER_IMAGE_EXECUTED_LOCALLY} do no exists! building it..."	
	docker build -f ./.gci/Dockerfile --target ${TARGET} -t ${DOCKER_IMAGE_EXECUTED_LOCALLY} --build-arg ARCH=${ARCH} .
else
	log_info "yeah! ${DOCKER_IMAGE_EXECUTED_LOCALLY} exists!!"
fi

docker run --rm -it -v $(pwd):/crono_tdc_py -v /dev:/dev --privileged ${DOCKER_IMAGE_EXECUTED_LOCALLY} /bin/bash
