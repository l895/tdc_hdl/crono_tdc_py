from pickle import FRAME
import serial
from typing import Union, NamedTuple
from crc import CrcCalculator, Crc16
import logging
import concurrent.futures
import queue
import codecs

FRAME_DELIMITER_CHAR = 0x7E
ESCAPE_CHAR = 0x7D

CONTROL_CHARS = [ESCAPE_CHAR, FRAME_DELIMITER_CHAR]

class HDLCLiteFrame(NamedTuple):
    data: bytes
    crc: bytes

class HDLCLiteProcessor:
    def __init__(self, uart_dev_path:str, uart_baudrate:int=115200, uart_rx_timeout_s:int=5, uart_stopbits:int=1):
        """
        Creates a HDLCLiteProcessor object. 
        
        Parameters
        ----------
        uart_dev_path : str
            The path of the serial device used to communicate Through 
            HDLC-lite frames (i.e. '/dev/ttyUSB0')

        uart_baudrate : int
            The baudrate of the uart (i.e. 115200)

        uart_rx_timeout_s : int
            The timeout of the receptions in when reading the RX buffer of the (in seconds).

        uart_stopbits : int
            The quantity of stopbits of the UART.
        """
        self.crc_calculator = CrcCalculator(Crc16.CCITT)
        self.uart_config = {
            "dev_path" : uart_dev_path,
            "baudrate" : uart_baudrate,
            "rx_timeout_s" : uart_rx_timeout_s,
            "stopbits" : uart_stopbits
        }
        logging.info(f'creating HDLC-lite Processor with cfg: {self.uart_config}')

    def _escape_frame(self, frame: bytes) -> bytes:
        """
        Escape a frame, replacing every special char with an escape char and followed
        by the XORed byte.

        Parameters
        ----------
        frame : bytes
            The frame that will be escaped.
        """
        logging.debug(f"escaping frame {frame} (0x{int.from_bytes(frame,'big'):X})")
        escaped_frame = bytearray()
        for b in frame:
            if (b == FRAME_DELIMITER_CHAR) or (b == ESCAPE_CHAR):
                escaped_frame.extend([ESCAPE_CHAR])
                escaped_frame.extend([b ^ 0x20])
            else:
                escaped_frame.extend([b])
        return escaped_frame

    def _unescape_frame(self, frame: bytes) -> bytes:
        """
        Unescape a frame, removing every control char and xor'ing the following.

        Parameters
        ----------
        frame : bytes
            The frame that will be escaped.
        """
        logging.debug(f"unescaping frame {frame} (0x{int.from_bytes(frame,'big'):X})")
        unescaped_frame = bytearray()
        xor_byte = False
        for b in frame:
            if b == ESCAPE_CHAR:
                xor_byte = True
            else:
                byte_to_store = b ^ 0x20 if xor_byte else b
                unescaped_frame.extend([byte_to_store])
                xor_byte = False
        return unescaped_frame

    def build_frame(self, data: bytes) -> bytes:
        """
        Build a hdlc-lite frame with a certain data field. 
        This method will compute the CRC16-CCITT of the data and then
        will escape any char that must be escaped.

        Parameters
        ----------
        data : bytes
            The data that wants to be sent in the frame.
        """
        logging.debug(f"building frame with data: {data} (0x{int.from_bytes(data,'big'):X})")
        checksum = self.crc_calculator.calculate_checksum(data)
        data_with_crc = bytearray()
        data_with_crc.extend(data)
        data_with_crc.extend(checksum.to_bytes(2,'big'))
        logging.debug(f"data with crc: {data_with_crc} (0x{int.from_bytes(data_with_crc,'big'):X})")

        data_with_crc_escaped = self._escape_frame(bytes(data_with_crc))
        logging.debug(f"data with crc escaped: {data_with_crc_escaped} (0x{int.from_bytes(data_with_crc_escaped,'big'):X})")

        frame = bytearray()
        frame.extend(FRAME_DELIMITER_CHAR.to_bytes(1,'big'))
        frame.extend(data_with_crc_escaped)
        frame.extend(FRAME_DELIMITER_CHAR.to_bytes(1,'big'))

        logging.debug(f"built frame: {frame} (0x{int.from_bytes(frame,'big'):X})")
        return frame

    def send_frame(self, frame: bytes):
        """
        Send a frame through UART.

        Parameters
        -----------
        frame: bytes
            The frame that will be sent.
        """
        with serial.Serial(
            port=self.uart_config['dev_path'],
            baudrate=self.uart_config['baudrate'],
            timeout=self.uart_config['rx_timeout_s'],
            stopbits=self.uart_config['stopbits']) as ser:
            ## sending frame through uart
            logging.info(f'sending frame {frame} through UART')
            ser.write(frame)

    def _receive_frame_with_serial(self, serial_object) -> Union[HDLCLiteFrame, None]:
        """ Method to receive a frame with a py-serial object. """
        ## first receive the start frame delimiter
        rx_byte = serial_object.read(1)
        while( rx_byte != b'\x7E' ):
            rx_byte = serial_object.read(1)

        logging.debug('start frame delimiter received!')
        frame_content = serial_object.read_until(expected=FRAME_DELIMITER_CHAR.to_bytes(1, 'big')).rstrip(FRAME_DELIMITER_CHAR.to_bytes(1, 'big'))
        logging.debug('end frame delimiter received! ')

        if len(frame_content) < 2:
            received_frame = None
            logging.debug(f"invalid frame format! len < 2 (min 3 bytes, 2 for CRC and 1 for DATA) (0x{int.from_bytes(frame_content,'big'):X})")
        else:
            # unescape first the frame and the split data and crc
            unescaped_frame = self._unescape_frame(frame_content)
            received_frame = HDLCLiteFrame(data=unescaped_frame[:-2], crc = unescaped_frame[-2:])
            logging.debug(f"frame data: {received_frame.data} (0x{int.from_bytes(received_frame.data,'big'):X}), frame CRC: 0x{int.from_bytes(received_frame.crc,'big'):X}")

        return received_frame

    def send_and_receive_frames_until(self, frame_to_send: bytes, receive_until:set) -> list:
        """ 
        Send a frame and receive frames until a certain ascii string is received in
        any of the frames.
        
        Parameters
        ----------
        receive_until: set
            Set of stop-condition data. If any received frame have any data
            that is contained in this set, stop the reception of frames. The elements
            of this set must be strings!.
        """
        logging.info(f"receiving frame until any of the following data is received: {receive_until}")
        frames_received = list()

        with serial.Serial(
            port=self.uart_config['dev_path'],
            baudrate=self.uart_config['baudrate'],
            timeout=self.uart_config['rx_timeout_s'],
            stopbits=self.uart_config['stopbits']) as ser:

                ## sending frame through uart
                logging.info(f'sending frame {frame_to_send} through UART')
                ser.write(frame_to_send)

                stop_condition_reached = False
                while stop_condition_reached == False:
                    received_frame = self._receive_frame_with_serial(ser)
                    if received_frame:
                        calculated_checksum = self.crc_calculator.calculate_checksum(received_frame.data)
                        if calculated_checksum.to_bytes(2, 'big') != received_frame.crc:
                            logging.warning(f"CRC calculated is different from the received! expected=0x{calculated_checksum:04X}, received=0x{int.from_bytes(received_frame.crc, 'big'):04X}")
                        else:
                            logging.debug('CRC is OK!')
                        
                        frames_received.append(received_frame)
                        if received_frame.data.isascii():
                            ascii_data = codecs.decode(received_frame.data,'ascii')
                            logging.debug(f"received frame data ({ascii_data}) is ascii! checking if it is stop-condition...")
                            for stop_cond in receive_until:
                                if stop_cond == ascii_data:
                                    logging.debug(f"received stop-condition frame! {ascii_data}")
                                    stop_condition_reached = True
                                    break
                            
                            # if don't break ascii frame is data anyways!
                            logging.debug(f"received frame is ascii but is data anyways!!! store and continue...")
                        else:
                            logging.debug(f"received frame is not ascii!! store and continue...")
                        
                return frames_received

    def _blocking_frame_transaction(self, frame: bytes) -> Union[HDLCLiteFrame, None]:
        """
        Send a frame through UART and wait for another frame in blocking fashion.

        Parameters
        ----------
        frame : bytes 
            The frame that will be sent.
        """
        with serial.Serial(
            port=self.uart_config['dev_path'],
            baudrate=self.uart_config['baudrate'],
            timeout=self.uart_config['rx_timeout_s'],
            stopbits=self.uart_config['stopbits']) as ser:
                ## sending frame through uart
                logging.info(f'sending frame {frame} through UART')
                ser.write(frame)

                received_frame = self._receive_frame_with_serial(ser)

                if received_frame:
                    calculated_checksum = self.crc_calculator.calculate_checksum(received_frame.data)
                    if calculated_checksum.to_bytes(2, 'big') != received_frame.crc:
                        logging.warning(f"CRC calculated is different from the received! expected=0x{calculated_checksum:04X}, received=0x{int.from_bytes(received_frame.crc, 'big'):04X}")
                    else:
                        logging.debug('CRC is OK!')
                    return received_frame

    def frame_transaction(self, frame: bytes, timeout_s:int) -> Union[HDLCLiteFrame, None]:
        """
        Send a frame through UART and wait for another frame and raises an exception if
        the timeout is detected.
        The frame transaction is launched in background with another process and the
        foreground process monitors the process timeout.

        Parameters
        ----------
        frame : bytes 
            The frame that will be sent.

        timeout_s : int 
            The timeout of the transaction.
        """
        logging.debug("launching frame transaction in background...")
        with concurrent.futures.ProcessPoolExecutor(max_workers=1) as executor:
            future_to_transaction = executor.submit(self._blocking_frame_transaction,frame)
            try:
                return future_to_transaction.result(timeout=timeout_s)
            except concurrent.futures._base.TimeoutError:
                logging.error(f"transaction took too long... ({timeout_s} seconds in total)!")
                for pid, process in executor._processes.items():
                    logging.error(f"terminating process {pid}...")
                    process.terminate()
                executor.shutdown()
