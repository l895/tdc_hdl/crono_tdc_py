from enum import IntEnum
from math import ceil
import logging
from typing import NamedTuple

TDC_CMD_ID_WIDTH_BYTES = 3
TDC_CMD_DATA_WIDTH_BYTES = 61

class CronoTDCCommandsError(Exception):
    pass

# ID of the TDC commands
class CMD_ID(IntEnum):
    READ_CRONO_TDC_CORE_VERSION = 1
    MEASURE                     = 2
    READ_TIME_WINDOWS_US        = 3
    WRITE_TIME_WINDOWS_US       = 4
    READ_CHANNELS_ENABLED       = 5
    WRITE_CHANNELS_ENABLED      = 6
    READ_CHANNELS_AVAILABLE     = 7
    READ_CHANNELS_DATA          = 8

# We want a look-up table with the commands of the TDC and some attributes
# of them. But, we want this list unmutable, because do not must to 
# change its values at runtime!
# Named tuple with some characteristics of the commands
class CronoTDCCommand(NamedTuple):
    name: str
    id: int

class CronoTDCCommandsLUT(NamedTuple):
    read_crono_tdc_core_version : CronoTDCCommand
    measure : CronoTDCCommand
    read_time_windows_us : CronoTDCCommand
    write_time_windows_us : CronoTDCCommand
    read_channels_enabled : CronoTDCCommand
    write_channels_enabled : CronoTDCCommand
    read_channels_available : CronoTDCCommand
    read_channels_data : CronoTDCCommand

class CronoTDCCommandProcessor():
    def __init__(self):
        '''
        Command processor constructor. Here we creates the LUT with all the commands
        of the TDC.
        '''
        self.cmds_lut = CronoTDCCommandsLUT(
            read_crono_tdc_core_version = CronoTDCCommand(name='read_crono_tdc_core_version', id=CMD_ID.READ_CRONO_TDC_CORE_VERSION),
            measure                     = CronoTDCCommand(name='measure',                     id=CMD_ID.MEASURE),
            read_time_windows_us        = CronoTDCCommand(name='read_time_windows_us',        id=CMD_ID.READ_TIME_WINDOWS_US),
            write_time_windows_us       = CronoTDCCommand(name='write_time_windows_us',       id=CMD_ID.WRITE_TIME_WINDOWS_US),
            read_channels_enabled       = CronoTDCCommand(name='read_channels_enabled',       id=CMD_ID.READ_CHANNELS_ENABLED),
            write_channels_enabled      = CronoTDCCommand(name='write_channels_enabled',      id=CMD_ID.WRITE_CHANNELS_ENABLED),
            read_channels_available     = CronoTDCCommand(name='read_channels_available',     id=CMD_ID.READ_CHANNELS_AVAILABLE),
            read_channels_data          = CronoTDCCommand(name='read_channels_data',          id=CMD_ID.READ_CHANNELS_DATA),
        )

    def look4cmd(self, cmd_name: str, not_exists_ok=False):
        """ Look for a command in the command look up table. """
        cmd = None
        try:
            cmd = self.cmds_lut._asdict()[cmd_name.lower()]
            logging.debug(f"cmd: {cmd_name} found!")
        except KeyError as excp:
            if not_exists_ok:
                logging.error(f"command {cmd_name} do not exists...")
            else:
                raise CronoTDCCommandsError(f'the command {cmd_name.lower()} is not valid! valids commands are: {self.cmds_lut._fields} {excp}')
        return cmd

    def build_cmd(self, cmd_name: str, cmd_data: bytes) -> bytes:
        """ 
        Build a TDC command with a determined ID and DATA and returns it as bytes.
        """
        cmd_id = self.look4cmd(cmd_name).id
        int_rep = cmd_id | (int.from_bytes(cmd_data,'big') << TDC_CMD_ID_WIDTH_BYTES*8)
        return int_rep.to_bytes(ceil(int_rep.bit_length()/8), 'big')
