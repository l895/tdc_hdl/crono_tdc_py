import logging
from typing import Union
import pandas as pd

from crono_tdc_py.cmd_processor import CronoTDCCommandProcessor
from crono_tdc_py.hdlc_lite_processor import HDLCLiteProcessor

MEASURE_TAG_LUT = {
    0b001 : {'name': "only_small_pulse"},
    0b010 : {'name': "long_pulse_starting"},
    0b011 : {'name': "long_pulse_starting_small_appears"},
    0b100 : {'name': "long_pulse_ending"},
    0b101 : {'name': "long_pulse_ending_small_appears"},
    0b110 : {'name': "long_pulse_ending_long_starting"},
    0b111 : {'name': "unknown"},
}

HDLC_LITE_TX_DATA_BUFF_WIDTH=13*8

class CronoTDCError(Exception):
    pass

class CronoTDC():
    def __init__(self, serial_port_dev: str, read_register_timeout_s: int=10):
        """
        Crono TDC class construct.
        
        Parameters
        ----------
        serial_port_dev : str
            The path of the serial port that is being used to communicate
            with Crono TDC.

        read_register_timeout_s : int
            Timeout of read register operation in seconds.
        """
        self.hdlc_lite_processor = HDLCLiteProcessor(serial_port_dev)
        self.cmd_processor = CronoTDCCommandProcessor()
        self.read_register_timeout_s = read_register_timeout_s

    def _read_register(self, reg_name: int, timeout_s:int) -> bytes:
        """
        Read a Crono TDC register.

        Parameters
        ----------
        reg_name : str
            The name of the command that must be read.
        """
        cmd_name = 'READ_' + reg_name
        cmd = self.cmd_processor.build_cmd(cmd_name=cmd_name, cmd_data = b'')
        frame = self.hdlc_lite_processor.build_frame(cmd)
        received_frame = self.hdlc_lite_processor.frame_transaction(frame,timeout_s)
        if received_frame:
            logging.debug(f"register value:\n" + \
            f"\thexadecimal = (0x{int.from_bytes(received_frame.data,'big'):X})\n" + \
            f"\tbytes = {received_frame.data} \n" + \
            f"\tdecimal = {int.from_bytes(received_frame.data,'big')}"
            )
            return received_frame.data
        else:
            logging.warning(f"no response frame received!")
            return b''

    def _write_register(self, reg_name: int, reg_val: Union[bytes, int], reg_width: int) -> bytes:
        """
        Write a Crono TDC register with some value.

        Parameters
        ----------
        reg_name : str
            The name of the command that must be read.

        reg_val : Union[bytes, int]
            The value to be written in the register. Could be an integer or a bytes object.
        
        reg_width: int
            The width of the register to be written. 
            FIXME: in the future this must be inside the class.
        """
        cmd_name = 'WRITE_' + reg_name

        if isinstance(reg_val, int):
            cmd_data = reg_val.to_bytes(reg_width,'big')
        else: # must be a byte if we reach here
            if len(reg_val) > reg_width:
                cmd_data = reg_val[reg_width:]
            else:
                cmd_data = reg_val

        cmd = self.cmd_processor.build_cmd(cmd_name=cmd_name, cmd_data = cmd_data)
        frame = self.hdlc_lite_processor.build_frame(cmd)
        self.hdlc_lite_processor.send_frame(frame)

    @property
    def crono_tdc_core_version(self) -> bytes:
        """
        Retrieve the Crono TDC Core version from FPGA.
        """
        return self._read_register('CRONO_TDC_CORE_VERSION', self.read_register_timeout_s)

    @property
    def channels_available(self) -> bytes:
        """
        Retrieve the channels available from FPGA.
        """
        mask = int.from_bytes(self._read_register('CHANNELS_AVAILABLE', self.read_register_timeout_s), 'big')
        channels_available_str = 'channels available: '
        for bit in range(mask.bit_length()):
            if (mask >> bit) & 0x1:
                channels_available_str = channels_available_str + f" {bit}"

        logging.info(channels_available_str)
        return mask

    @property
    def time_windows_us(self) -> bytes:
        """
        Retrieve actual time windows from FPGA.
        """
        return int.from_bytes(self._read_register('TIME_WINDOWS_US', self.read_register_timeout_s), 'big')

    @time_windows_us.setter
    def time_windows_us(self, time_us: int) -> bytes:
        """
        Set the time windows to FPGA.

        Parameters
        ----------
        time_us : int
            The width in micro-seconds of the measuring time windows.

        """
        return self._write_register('TIME_WINDOWS_US', reg_val = time_us, reg_width = 4)

    @property
    def channels_enabled(self) -> bytes:
        """
        Retrieve actual enabled channels from FPGA.
        """
        mask = int.from_bytes(self._read_register('CHANNELS_ENABLED', self.read_register_timeout_s), 'big')
        channels_enabled_str = 'channels enabled:'
        for bit in range(mask.bit_length()):
            if (mask >> bit) & 0x1:
                channels_enabled_str = channels_enabled_str + f' {bit}'
        
        logging.info(channels_enabled_str)

        return mask

    @channels_enabled.setter
    def channels_enabled(self, mask: int) -> bytes:
        """
        Config channels enabled register in FPGA. The value of this register
        is a mask of channels. If bit 'i' is `1` then the channel `i` is enabled.

        Parameters
        ----------
        channels_enabled_mask : int
            The value that will be written to this register.
        """
        return self._write_register('CHANNELS_ENABLED', reg_val = mask, reg_width = 2)

    def measure(self) -> None:
        """
        Execute measure command.
        """
        actual_time_windows_us = self.time_windows_us
        transaction_timeout_s = int(self.time_windows_us*2)/10000 if actual_time_windows_us > 1000000 else 5
        logging.debug(f"transaction timeout: {transaction_timeout_s} s")

        cmd_name = 'MEASURE'
        cmd = self.cmd_processor.build_cmd(cmd_name=cmd_name, cmd_data = b'')
        frame = self.hdlc_lite_processor.build_frame(cmd)
        received_frame = self.hdlc_lite_processor.frame_transaction(frame,transaction_timeout_s)
        if received_frame:
            logging.info(f"received frame: {received_frame.data}")
        else:
            logging.warning(f"no response frame received!")

    def _convert_channels_data_frames_to_csv(self, channels_data_frames: set, csv_filename: str):
        """ Convert a set of channels data frames to a csv with just two columns:
            | channel | data_register | clock_cycle_num | iserdese_out | tag |
        """
        df = pd.DataFrame(columns=["channel", "data_register", "clock_cycle_num", "iserdese_out", "tag"])

        for frame in channels_data_frames:
            frame_integerized = int.from_bytes(frame.data,'big')
            logging.info(f"analyzing frame {frame_integerized:064X}...")
            channel = (frame_integerized >> (HDLC_LITE_TX_DATA_BUFF_WIDTH-16)) & 0xFFFF
            data_registers_qty = (frame_integerized >> (HDLC_LITE_TX_DATA_BUFF_WIDTH-24)) & 0xFF

            data_register = 0
            for i in range(data_registers_qty):
                data_register = (frame_integerized >> (HDLC_LITE_TX_DATA_BUFF_WIDTH-24-((i+1)*20))) & 0xFFFFF

                clock_cycle_num = (data_register >> 7) & 0xFFF
                iserdese_out = (data_register >> 3) & 0xF
                tag = MEASURE_TAG_LUT[(data_register & 0b111)]['name']

                logging.info(
                    f"channel: {channel}, data_register: {data_register:05X}, "
                    f"clock_cycle_num: {clock_cycle_num}, iserdese_output: {iserdese_out:04b}, "
                    f"tag: {tag}")

                new_row_df = pd.DataFrame(
                    {
                        "channel": [channel], "data_register": [data_register],
                        "clock_cycle_num": [clock_cycle_num], "iserdese_out": [iserdese_out],
                        "tag": [tag]
                    }
                )
#                logging.info("new row df: " + new_row_df.to_string())
                df = pd.concat([df, new_row_df],ignore_index = True, axis = 0)
#                logging.info("df: " + df.to_string())

        df.sort_values(by=['clock_cycle_num'], inplace=True)
        df.to_csv(csv_filename)
        logging.info(f"csv stored as {csv_filename}!")

    def read_channels_data(self, csv_filename: str) -> None:
        """
        Execute read_channels_data command.
        """
        cmd_name = 'READ_CHANNELS_DATA'
        cmd = self.cmd_processor.build_cmd(cmd_name=cmd_name, cmd_data = b'')
        frame = self.hdlc_lite_processor.build_frame(cmd)

        channels_data_frames = self.hdlc_lite_processor.send_and_receive_frames_until(frame_to_send=frame, receive_until={"enaread", "noena"})
        self._convert_channels_data_frames_to_csv(channels_data_frames, csv_filename)
